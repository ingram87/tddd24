/*
	This javascript file will contain:
	* functionality:
		* displayView()
		* document.onload()
 */


/*
	GLOBAL VARIABLES
 */
var state = new String("welcome");
var userSignupData = new Object();
var uFirstname = null;
var uFamilyname = null;
var uCity = null;
var uCountry = null;
var uGender = null;
var uEmail = null;
var uPass1 = null;
var uPass2 = null;
var validSignUpInput = null;
var validSignInInput = null;
var signinRes;
var inEmail = null;
var inPass = null;
// serverside login data
var userToken = null;
var signinMessage = null;
var tab = null;
var viewname = null;
var targetEmail = null;

var errorClassList = new Array();

// the code required to display a view
// code that is executed as the page is loaded
window.onload = function() {
	userToken = localStorage.token;
	if(localStorage.email == serverstub.tokenToEmail(userToken)) {
		signinByToken(userToken);
		tabSelect(localStorage.tab);
	} else {
		viewname = "welcomepage";
		displayView();
	}
	
}

var signupData = function() {
	// obtain data from inputs
	uFirstname = document.getElementById("firstname").value;
	uFamilyname = document.getElementById("familyname").value;
	uCity = document.getElementById("city").value;
	uCountry = document.getElementById("country").value;
	uGender = document.getElementById("gender").value;
	uEmail = document.getElementById("email").value;
	uPass1 = document.getElementById("password1").value;
	uPass2 = document.getElementById("password2").value;
	
	// check if the signup inputs are valid
	clientValidation();

	if (validSignUpInput == true) {
		userSignupData = {
		"firstname": uFirstname,
		"familyname": uFamilyname,
		"city": uCity,
		"country": uCountry,
		"gender": uGender,
		"email": uEmail,
		"password": uPass1
		}

		alert(serverstub.signUp(userSignupData).message);
		if (serverstub.signUp(userSignupData).success == true) {
			var signupRes = serverstub.signIn(uEmail, uPass1);
			userToken = signupRes.data;
			signinByToken(signupRes.data);
		}
	}
	
} 


var signinData = function() {
	inEmail = document.getElementById("loginEmail").value;
	inPass = document.getElementById("loginPass").value;

	// Check if the sign in data inputs are valid
	if ((inEmail != "") && (inPass != "")) {
		validSignInInput = true;
		remErrorClass("loginEmail");
		remErrorClass("loginPass");
	} else {
		validSignInInput = false;
		addErrorClass("loginEmail");
		addErrorClass("loginPass");
	}

	if (validSignInInput == true) {
		// Signin on the server side
		signinRes = serverstub.signIn(inEmail, inPass);
		if (signinRes.success == true) {
			userToken = signinRes.data;
			alert(signinRes.message);
			signinByToken(userToken);
			remErrorClass("loginEmail");
			remErrorClass("loginPass");
			
		} else {
			addErrorClass("loginEmail");
			addErrorClass("loginPass"); 
			alert(signinRes.message);
		}
	}
}

var signinByToken = function(tok) {
	localStorage.token = userToken;
	localStorage.email = serverstub.tokenToEmail(userToken);
	if ((userToken != "") || (userToken != null)) {
		viewname = "userpage";
		displayView();
	}
	displayViewAccountTab();
}

var addErrorClass = function(idname) {
	// no previous error registered 
	if (errorClassList.indexOf(idname) == -1) {
		errorClassList.unshift(idname);
		document.getElementById(idname).value = "";
		document.getElementById(idname).className = document.getElementById(idname).className + " error";
	} 
}

var remErrorClass = function(idname) {
	// if there is an error registered before
	if (errorClassList.indexOf(idname) != -1) {
		errorClassList.splice(errorClassList.indexOf(idname), 1);
		document.getElementById(idname).className = document.getElementById(idname).className.replace(" error", "");
	}
}

var displayViewAccountTab = function() {
	document.getElementById("accountText").innerHTML = serverstub.tokenToEmail(userToken) + "'s Home page.";
}

var displayViewHomeTab = function() {
	if (tab == "home") {
		var currentUserInfo = serverstub.getUserDataByToken(userToken).data;
		document.getElementById("homeDivUserEmail").innerHTML = "Email: " + currentUserInfo.email;
		document.getElementById("homeDivUserName").innerHTML = "Name: " + currentUserInfo.firstname + " " + currentUserInfo.familyname;
		document.getElementById("homeDivUserGender").innerHTML = "Gender: " + currentUserInfo.gender;
		document.getElementById("homeDivUserCity").innerHTML = "City: " + currentUserInfo.city;
		document.getElementById("homeDivUserCountry").innerHTML = "Country: " + currentUserInfo.country;		
	} 
}

var reloadMsgWall = function() {
	var msgWall = null;
	var wall = null;
	if (tab == "home") {
		msgWall = serverstub.getUserMessagesByToken(userToken);
		wall = document.getElementById("homeMsgWallDiv");
	} else if (tab == "browse") {
		msgWall = serverstub.getUserMessagesByEmail(userToken, targetEmail);
		wall = document.getElementById("browseMsgWallDiv");
	}
	wall.innerHTML = "";
	for (var j=0; j<msgWall.data.length; j++) {
		
		wall.innerHTML += "From: " + (msgWall.data[j].writer) + " ==> " +(msgWall.data[j].content) + "<br>";
	}

	
}


var postMessage = function() {
	var postMessageRes = null;
	if (tab == "home") {
		postMessageRes = serverstub.postMessage(userToken, document.getElementById("homePostMsgTextarea").value, serverstub.tokenToEmail(userToken));
		alert(postMessageRes.message);
	} else if (tab == "browse") {
		postMessageRes = serverstub.postMessage(userToken, document.getElementById("browsePostMsgTextarea").value, targetEmail);
		alert(postMessageRes.message);
	}
	
}


var displayView = function() {
	if (viewname == "userpage") {
		document.getElementById('welcomeviewarea').innerHTML = document.getElementById('profileview').innerHTML;
	} else if (viewname == "welcomepage") {
		document.getElementById('welcomeviewarea').innerHTML = document.getElementById('welcomeview').innerHTML;
	}
}

var logout = function() {
	var signOutRes = serverstub.signOut(userToken);
	alert(signOutRes.message);
	localStorage.token ="";
	viewname = "welcomepage";
	displayView();
}

var changeTarget = function() {

	targetEmail = document.getElementById('targetInput').value;

	var targetData = serverstub.getUserDataByEmail(userToken, targetEmail);

	if (targetData.success == true) {
		var targetUserInfo = serverstub.getUserDataByEmail(userToken, targetEmail).data;
		//alert(serverstub.getUserDataByEmail(userToken, targetEmail).success);
		document.getElementById("browseDivUserEmail").innerHTML = "Email: " + targetUserInfo.email;
		document.getElementById("browseDivUserName").innerHTML = "Name: " + targetUserInfo.firstname + " " + targetUserInfo.familyname;
		document.getElementById("browseDivUserGender").innerHTML = "Gender: " + targetUserInfo.gender;
		document.getElementById("browseDivUserCity").innerHTML = "City: " + targetUserInfo.city;
		document.getElementById("browseDivUserCountry").innerHTML = "Country: " + targetUserInfo.country;

		reloadMsgWall();
	} else {
		alert(targetData.message);
	}
	
}

var tabSelect = function(tabName) {
	if (tabName == 'home') {
		tab = "home";
		localStorage.tab = "home";
		document.getElementById('Home').style.display = "block";
		document.getElementById('Browse').style.display = "none";
		document.getElementById('Account').style.display = "none";
		displayViewHomeTab(); 
		reloadMsgWall();
	} else if (tabName == 'browse') {
		tab = "browse";
		localStorage.tab = "browse";
		document.getElementById('Home').style.display = "none";
		document.getElementById('Browse').style.display = "block";
		document.getElementById('Account').style.display = "none";
		reloadMsgWall();
	} else if (tabName == 'account') {
		tab = "account";
		localStorage.tab = "account";
		document.getElementById('Home').style.display = "none";
		document.getElementById('Browse').style.display = "none";
		document.getElementById('Account').style.display = "block";
	}
	//document.getElementById(tabName).display
}

var newpass = function(formData) {
	if ((formData.oldPassword.value != "") && (formData.newPassword1.value != "") && (formData.newPassword1.value == formData.newPassword2.value)) {
		var changePassRes = serverstub.changePassword(userToken, formData.oldPassword.value, formData.newPassword1.value);
		alert(changePassRes.message);
	}
}


// specified validation for sign-up inputs

var clientValidation = function() {
	var totalError = new Array();
	// First name
	if (uFirstname == null || uFirstname == "") {
		addErrorClass("firstname");
		totalError[0] = "false";
	} else {
		remErrorClass("firstname");
		totalError[0] = "true";
	}
	// Family name
	if (uFamilyname == null || uFamilyname == "") {
	 	addErrorClass("familyname");
	 	totalError[1] = "false";
	} else {
		totalError[1] = "true";
		remErrorClass("familyname");
	}
	// City
	if (uCity == null || uCity == "") {
		addErrorClass("city");
	 	totalError[2] = "false";
	} else {
		totalError[2] = "true";
		remErrorClass("city");
	}
	// Country
	if (uCountry == null || uCountry == "") {
	 	addErrorClass("country");
	 	totalError[3] = "false";
	} else {
		totalError[3] = "true";
		remErrorClass("country");
	}
	// Email
	if (uEmail == null || uEmail == "") {
		addErrorClass("email");
	 	totalError[4] = "false";
	} else {
		totalError[4] = "true";
		remErrorClass("email");
	}
	// Password 1
	if (uPass1 == null || uPass1 == "") {
		addErrorClass("password1");
	 	totalError[5] = "false";
	} else {
		totalError[5] = "true";
		remErrorClass("password1");
	}
	// Password 2
	if (uPass2 == null || uPass2 == "") {
		addErrorClass("password2");
	 	totalError[6] = "false";
	} else {
		totalError[6] = "true";
		addErrorClass("password2");
	} 

	//if the passwords don't match
	if ((uPass1 != uPass2) && (uPass1 != "")){
		//alert("Password doesn't match, please check");
		totalError[7] = "false";
		addErrorClass("password1");
		addErrorClass("password2");
	} else {
		if ((uPass1 != "") && (uPass2 != "")){
			totalError[7] = "true";
			remErrorClass("password1");
			remErrorClass("password2");
		}
	}

	var ifError = 0;
	for (var i=0; i<totalError.length; i++) {
		if (totalError[i] == "false") {
			ifError = ifError + 1;
		}
	}
	
	if (ifError == 0) {
		validSignUpInput = true;
	} else {
		validSignUpInput = false;
	}
}




