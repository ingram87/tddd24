# Database_helper
import sqlite3
from flask import Flask, g

DATABASE = 'database.db'


def get_db():
    g.db = getattr(g, 'database.db', None)
    if g.db is None:
        g.db = sqlite3.connect(DATABASE)
    return g.db

def close_db():
    db = getattr(g, 'database.db', None)
    if db is not None:
        db.close()
    pass

# User related functions
# Input are SQL injection protected by Flask

def add_user(email, firstname, familyname, gender, city, country, password):
    db = get_db()
    cur = db.cursor()
    cur.execute("INSERT INTO users (email, firstname, familyname, gender, city, country, password) values (?,?,?,?,?,?,?)", (email, firstname, familyname, gender, city, country, password))
    db.commit()
    cur.close()
    close_db()

def change_password(password, email):
    db = get_db()
    cur = db.cursor()
    cur.execute("UPDATE users SET password=(?) WHERE email=(?)",  (password, email))
    db.commit()
    cur.close()
    close_db()

def get_user(email):
    db = get_db()
    cur = db.cursor()
    cur.execute('SELECT * FROM users WHERE email = ?', (email,))
    db.commit()
    userinfo = cur.fetchone()
    cur.close()
    close_db()
    return userinfo
    

def remove_user(email):
    db = get_db()
    cur = db.cursor()
    cur.execute('DELETE FROM users WHERE email = ?', (email,))
    db.commit()
    cur.close()
    close_db()


# Message related functions
def get_message(email):
    db = get_db()
    cur = db.cursor()
    cur.execute('SELECT * FROM messages WHERE toemail = ?', (email,))
    db.commit()
    messagepack = cur.fetchall()
    cur.close()
    close_db()
    return messagepack

def post_message(fromEmail, toEmail, message):
    db = get_db()
    cur = db.cursor() 
    cur.execute('INSERT INTO messages (toemail, message, fromemail) values (?,?,?)', (toEmail, message, fromEmail))
    db.commit()
    cur.close()
    close_db()


# token related functions
def add_token(token, email):
    db = get_db()
    cur = db.cursor()
    cur.execute("INSERT INTO tokens (token, email) values (?,?)", (token, email))
    db.commit()
    cur.close()
    close_db()

def remove_token(email):
    db = get_db()
    cur = db.cursor()
    cur.execute('DELETE FROM tokens WHERE email = ?', (email,))
    db.commit()
    cur.close()
    close_db()

def get_emailbytoken(token):
    db = get_db()
    cur = db.cursor()
    cur.execute('SELECT * FROM tokens where token = ?', (token,))
    db.commit()
    email = cur.fetchone()
    cur.close()
    close_db()
    return email[1]

# utilities
# check if the user already exist
def user_exist(email):
    db = get_db()
    cur = db.cursor()
    cur.execute('SELECT COUNT(*) FROM users WHERE email = ?', (email,))
    res = cur.fetchone()
    db.commit()
    cur.close()
    close_db()
    if res[0] == 1:
        return True
    else:
        return False

def user_loggedin(email):
    db = get_db()
    cur = db.cursor()
    cur.execute('SELECT COUNT(*) FROM tokens WHERE email = ?', (email,))
    res = cur.fetchone()
    db.commit()
    cur.close()
    close_db()
    if res[0] == 1:
        return True
    else:
        return False

def token_exist(token):
    db = get_db()
    cur = db.cursor()
    cur.execute('SELECT COUNT(*) FROM tokens WHERE token = ?', (token,))
    res = cur.fetchone()
    db.commit()
    cur.close()
    close_db()
    if res[0] == 1:
        return True
    else:
        return False


