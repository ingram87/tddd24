/*
	This javascript file will contain:
	* functionality:
		* displayView()
		* document.onload()
 */


/*
	GLOBAL VARIABLES
 */
var state = new String("welcome");
var userSignupData = new Object();
var uFirstname = null;
var uFamilyname = null;
var uCity = null;
var uCountry = null;
var uGender = null;
var uEmail = null;
var uPass1 = null;
var uPass2 = null;
var validSignUpInput = null;
var validSignInInput = null;
var signinRes;
var inEmail = null;
var inPass = null;
// serverside login data
var userToken = null;
var signinMessage = null;
var tab = null;
var viewname = null;
var targetEmail = null;
var Email = null;
var targetUserInfo = null;
var errorClassList = new Array();

// the code required to display a view
// code that is executed as the page is loaded
window.onload = function() {
	userToken = localStorage.token;
	if(localStorage.email == serverstub.tokenToEmail(userToken)) {
		signinByToken(userToken);
		tabSelect(localStorage.tab);
	} else {
        viewname = "welcomepage";
		displayView();
	}
	
}

/**********************
    HTML FUNCTIONS
***********************/

var signupData = function() {
	// obtain data from inputs
	uFirstname = document.getElementById("firstname").value;
	uFamilyname = document.getElementById("familyname").value;
	uCity = document.getElementById("city").value;
	uCountry = document.getElementById("country").value;
	uGender = document.getElementById("gender").value;
	uEmail = document.getElementById("email").value;
	uPass1 = document.getElementById("password1").value;
	uPass2 = document.getElementById("password2").value;
	
	// check if the signup inputs are valid
	clientValidation();

	if (validSignUpInput == true) {

        var params = "firstname=" + uFirstname + "&familyname=" + uFamilyname + "&city=" + uCity + "&country=" + uCountry + "&gender=" + uGender + "&email=" + uEmail + "&password=" + uPass1;
        console.log(params);

        http = new XMLHttpRequest();
        http.open('POST', '/signup', true);
        http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        http.onreadystatechange = function() {
            if (http.readyState == 4 && http.status == 200) {
                var resp = JSON.parse(http.responseText);
                displayAlert('Welcome', resp.message);
            }
        }
        http.send(params);

    } else {
		displayAlert('Welcome', "Sign up inputs are not valid, check again.");
	}
} 


var signinData = function() {
	inEmail = document.getElementById("loginEmail").value;
	inPass = document.getElementById("loginPass").value;

	// Check if the sign in data inputs are valid
	if ((inEmail != "") && (inPass != "")) {
		validSignInInput = true;
		remErrorClass("loginEmail");
		remErrorClass("loginPass");
	} else {
		validSignInInput = false;
		addErrorClass("loginEmail");
		addErrorClass("loginPass");
		displayAlert('Welcome', "Login inputs are not valid!");
	}

	if (validSignInInput == true) {
        var params = "email="+inEmail+"&password="+inPass;
		// Signin on the server side
        http = new XMLHttpRequest();
        http.open('POST', '/signin', true);
        http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        http.onreadystatechange = function() {
            if (http.readyState == 4 && http.status == 200) {
                var resp = JSON.parse(http.responseText);
                console.log(resp);
                if (resp.success == true) {
                    userToken = resp.token;
                    signinByToken(userToken);
                    remErrorClass("loginEmail");
                } else {
                    addErrorClass("loginEmail");
			        addErrorClass("loginPass");
			        displayAlert('Welcome', resp.message);
                }
            }
        }
        http.send(params);
	}
}

var logout = function() {
    var params = "token="+userToken;
    http = new XMLHttpRequest();
    http.open('POST', '/signout', true);
    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    http.onreadystatechange = function() {
        if (http.readyState == 4 && http.status == 200) {
            var res = JSON.parse(http.responseText);
            if (res.success == true) {
                localStorage.token = "";
	            localStorage.email = "";
	            viewname = "welcomepage";
	            localStorage.tab = "";
	            displayView();
                displayAlert('Welcome', res.message);
            } else {
                displayAlert('userpage', res.message);
            }

        }
    }
    http.send(params);
}


var displayUserInfo = function() {
    var params = "token="+userToken;
    http = new XMLHttpRequest();
    http.open('POST', '/getuserdatabytoken', true);
    http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    http.onreadystatechange = function() {
        if (http.readyState == 4 && http.status == 200) {
            currentUserInfo = JSON.parse(http.responseText).object;
            alert(JSON.parse(http.response).success);
            displayAlert('userpage', JSON.parse(http.responseText).message);
        }
    }
    http.send(params);

	if (tab == "home") {
		document.getElementById("homeDivUserEmail").innerHTML = "Email: " + currentUserInfo.email;
		document.getElementById("homeDivUserName").innerHTML = "Name: " + currentUserInfo.firstname + " " + currentUserInfo.familyname;
		document.getElementById("homeDivUserGender").innerHTML = "Gender: " + currentUserInfo.gender;
		document.getElementById("homeDivUserCity").innerHTML = "City: " + currentUserInfo.city;
		document.getElementById("homeDivUserCountry").innerHTML = "Country: " + currentUserInfo.country;		
	} else if (tab == "browse") {
		if((targetUserInfo != "")&&(targetUserInfo != null)){
			document.getElementById("homeDivUserEmail").innerHTML = "Email: " + targetUserInfo.email;
			document.getElementById("homeDivUserName").innerHTML = "Name: " + targetUserInfo.firstname + " " + targetUserInfo.familyname;
			document.getElementById("homeDivUserGender").innerHTML = "Gender: " + targetUserInfo.gender;
			document.getElementById("homeDivUserCity").innerHTML = "City: " + targetUserInfo.city;
			document.getElementById("homeDivUserCountry").innerHTML = "Country: " + targetUserInfo.country;
		}
	}
}

var reloadMsgWall = function() {
	var msgWall = null;
	var wall = null;
	if (tab == "home") {
		msgWall = serverstub.getUserMessagesByToken(userToken);
		wall = document.getElementById("msgWall");
		wall.innerHTML = "";
		for (var j=0; j<msgWall.data.length; j++) {
			wall.innerHTML += "From: " + (msgWall.data[j].writer) + " ==> " +(msgWall.data[j].content) + "<br>";
		}

	} else if (tab == "browse") {
		msgWall = serverstub.getUserMessagesByEmail(userToken, targetEmail);
		wall = document.getElementById("msgWall");
		if ((targetEmail != "")&&(targetEmail != null)&&(msgWall.success == true)) {
			wall.innerHTML = "";
			for (var j=0; j<msgWall.data.length; j++) {
				wall.innerHTML += "From: " + (msgWall.data[j].writer) + " ==> " +(msgWall.data[j].content) + "<br>";
			}
		}
	}
		
}


var postMessage = function() {
	var postMessageRes = null;
	if (tab == "home") {
		postMessageRes = serverstub.postMessage(userToken, document.getElementById("postMsgInput").value, serverstub.tokenToEmail(userToken));
		reloadMsgWall();
		displayAlert('User', postMessageRes.message);
	} else if (tab == "browse") {
		postMessageRes = serverstub.postMessage(userToken, document.getElementById("postMsgInput").value, targetEmail);
		reloadMsgWall();
		displayAlert('User', postMessageRes.message);
	}
	
	document.getElementById("postMsgInput").value = "";
	
}


var displayView = function() {
	if (viewname == "userpage") {
		document.getElementById('contentAreaDiv').innerHTML = document.getElementById('profileview').innerHTML;
	} else if (viewname == "welcomepage") {
		document.getElementById('contentAreaDiv').innerHTML = document.getElementById('welcomeview').innerHTML;
	} else {
		document.getElementById('contentAreaDiv').innerHTML = document.getElementById('welcomeview').innerHTML;
	}
}




var changeTarget = function() {

	targetEmail = document.getElementById('targetChangeInput').value;
	var targetData = serverstub.getUserDataByEmail(userToken, targetEmail);

	if (targetData.success == true) {
		targetUserInfo = serverstub.getUserDataByEmail(userToken, targetEmail).data;
		displayAlert('User', targetData.message);
		document.getElementById("homeDivUserEmail").innerHTML = "Email: " + targetUserInfo.email;
		document.getElementById("homeDivUserName").innerHTML = "Name: " + targetUserInfo.firstname + " " + targetUserInfo.familyname;
		document.getElementById("homeDivUserGender").innerHTML = "Gender: " + targetUserInfo.gender;
		document.getElementById("homeDivUserCity").innerHTML = "City: " + targetUserInfo.city;
		document.getElementById("homeDivUserCountry").innerHTML = "Country: " + targetUserInfo.country;
		reloadMsgWall();
	} else {
		displayAlert('User', targetData.message);
	}
	
	document.getElementById('targetChangeInput').value = "";
	
}

var tabSelect = function(tabName) {
	if (tabName == "home") {
		tab = "home";
		localStorage.tab = "home";
		document.getElementById('Home').style.display = "block";
		document.getElementById('targetChangeDiv').style.display = "none";
		document.getElementById('Account').style.display = "none";
		displayUserInfo();
		reloadMsgWall();
	} else if (tabName == "browse") {
		tab = "browse";
		localStorage.tab = "browse";
		document.getElementById('Home').style.display = "block";
		document.getElementById('targetChangeDiv').style.display = "block";
		document.getElementById('Account').style.display = "none";
		displayUserInfo();
		reloadMsgWall();
	} else if (tabName == "account") {
		tab = "account";
		localStorage.tab = "account";
		document.getElementById('Home').style.display = "none";
		document.getElementById('targetChangeDiv').style.display = "none";
		document.getElementById('Account').style.display = "block";
	}

}

var newpass = function() {
	var oldp = document.getElementById("oldPassword").value;
	var new1 = document.getElementById("newPassword1").value;
	var new2 = document.getElementById("newPassword2").value;
	if ((oldp != "") && (new1 != "") && (new1 == new2)) {
		var changePassRes = serverstub.changePassword(userToken, oldp, new1);
		displayAlert('User', changePassRes.message);
	} 
	if ((new1 == "") || (new1 != new2) || (new2 == "")) {
		addErrorClass("newPassword1");
		addErrorClass("newPassword2");
		displayAlert('User', "New passwords are empty or they don't match, re-check please.");
	} else {
		remErrorClass("newPassword1");
		remErrorClass("newPassword2");
	}
	
	
	if ((oldp != "")&&(oldp != null)) {
		remErrorClass("oldPassword");
	} else {
		addErrorClass("oldPassword");
		displayAlert('User', "You need to fill in the old password.");
	}
}

/*****************************
    ULTILITIES FUNCTIONS
 *****************************/

var displayAlert = function(pageName, content) {
	if (pageName == "Welcome") {
		document.getElementById("warningMsgWelDiv").style.display = "block";
		document.getElementById("warningMsgWelDiv").innerHTML = content;
		setTimeout(function(){document.getElementById("warningMsgWelDiv").style.display = "none";}, 2000);
	} else if (pageName == "User") {
		document.getElementById("warningMsgUsrDiv").style.display = "block";
		document.getElementById("warningMsgUsrDiv").innerHTML = content;
		setTimeout(function(){document.getElementById("warningMsgUsrDiv").style.display = "none";}, 2000);
	}
}

// specified validation for sign-up inputs

var clientValidation = function() {
	var totalError = new Array();
	// First name
	if (uFirstname == null || uFirstname == "") {
		addErrorClass("firstname");
		totalError[0] = "false";
	} else {
		remErrorClass("firstname");
		totalError[0] = "true";
	}
	// Family name
	if (uFamilyname == null || uFamilyname == "") {
	 	addErrorClass("familyname");
	 	totalError[1] = "false";
	} else {
		totalError[1] = "true";
		remErrorClass("familyname");
	}
	// City
	if (uCity == null || uCity == "") {
		addErrorClass("city");
	 	totalError[2] = "false";
	} else {
		totalError[2] = "true";
		remErrorClass("city");
	}
	// Country
	if (uCountry == null || uCountry == "") {
	 	addErrorClass("country");
	 	totalError[3] = "false";
	} else {
		totalError[3] = "true";
		remErrorClass("country");
	}
	// Email
	if (uEmail == null || uEmail == "") {
		addErrorClass("email");
	 	totalError[4] = "false";
	} else {
		totalError[4] = "true";
		remErrorClass("email");
	}
	// Password 1
	if (uPass1 == null || uPass1 == "") {
		addErrorClass("password1");
	 	totalError[5] = "false";
	} else {
		totalError[5] = "true";
		remErrorClass("password1");
	}
	// Password 2
	if (uPass2 == null || uPass2 == "") {
		addErrorClass("password2");
	 	totalError[6] = "false";
	} else {
		totalError[6] = "true";
		addErrorClass("password2");
	} 

	//if the passwords don't match
	if ((uPass1 != uPass2) && (uPass1 != "")){
		//alert("Password doesn't match, please check");
		totalError[7] = "false";
		addErrorClass("password1");
		addErrorClass("password2");
	} else {
		if ((uPass1 != "") && (uPass2 != "")){
			totalError[7] = "true";
			remErrorClass("password1");
			remErrorClass("password2");
		}
	}

	var ifError = 0;
	for (var i=0; i<totalError.length; i++) {
		if (totalError[i] == "false") {
			ifError = ifError + 1;
		}
	}
	
	if (ifError == 0) {
		validSignUpInput = true;
	} else {
		validSignUpInput = false;
	}
}

var signinByToken = function(tok) {
	localStorage.token = userToken;
	localStorage.email = serverstub.tokenToEmail(userToken);
	if ((userToken != "") || (userToken != null)) {
		viewname = "userpage";
		displayView();
	}

}

var addErrorClass = function(idname) {
	// no previous error registered
	if (errorClassList.indexOf(idname) == -1) {
		errorClassList.unshift(idname);
		document.getElementById(idname).value = "";
		document.getElementById(idname).className = document.getElementById(idname).className + " error";
	}
}

var remErrorClass = function(idname) {
	// if there is an error registered before
	if (errorClassList.indexOf(idname) != -1) {
		errorClassList.splice(errorClassList.indexOf(idname), 1);
		document.getElementById(idname).className = document.getElementById(idname).className.replace(" error", "");
	}
}

