# this file shall contain all the server side functions

from flask import Flask, request, jsonify
import json
from flask_cors import cross_origin # support local usage without installed package
import database_helper
import string
import random
from werkzeug.security import generate_password_hash, check_password_hash
from geventwebsocket.handler import WebSocketHandler
from gevent.pywsgi import WSGIServer


# Flask init
app = Flask(__name__)
app.debug = True

#############
# FUNCTIONS #
#############

# generate a 128-bits long token with mixed characters and digits
def token_generator(size=128, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))




# a hello world function, print everything within the database
#@app.route('/', methods=['GET', 'POST'])
#@cross_origin()
#def hello():
#    return jsonify(message= "This is twidder server")

@app.route('/')
def root():
    return app.send_static_file("client.html")


# Authenticates the username by the provided password.
@app.route("/signin", methods=['POST'])
@cross_origin(origins='*')
def sign_in():
    _success=False
    _token=None
    email = request.form['email']
    if (database_helper.user_loggedin(email) == True):
        _message="User is already login."
    else:
        if database_helper.user_exist(email) == True:
            # A string value containing a randomly generated access token if the authentication is successful.
            if ((email != '') & (check_password_hash(database_helper.get_user(email)[6], request.form['password']) == True)):
                token = token_generator()
                # save token into the database
                database_helper.add_token(token, email)
                _token=token
                _success=True
                _message="Sign in successed!"
            else:
                _message="Password is wrong"
        else:
            _message="User doesn't exist"

    return jsonify(success=_success,
                   token=_token,
                   message=_message)

# Registers a user in the database.
@app.route("/signup", methods=['POST'])
@cross_origin()
def sign_up():
    _success=False

    if (request.method == 'POST'):

        email = request.form['email']
        firstname = request.form['firstname']
        familyname = request.form['familyname']
        gender = request.form['gender']
        city = request.form['city']
        country = request.form['country']
        password = request.form['password']
        phash = generate_password_hash(password)


    if ((email != '') & (firstname != '') & (familyname != '') & (gender != '') & (city != '') & (country != '') & (password != '')):
        # check if user is already signed up
        res = database_helper.user_exist(email)
        if (res == False):
            database_helper.add_user(email, firstname, familyname, gender, city, country, phash)
            _message="Sign up successed!"
            _success=True
        else:
            _message="user already exist"
    else:
        _message="some args are empty"

    return jsonify(success=_success,
                   message=_message)

# Signs out a user from the system.
@app.route("/signout", methods=['POST'])
@cross_origin()
def sign_out():
    _success=False
    _message = False
    #remove the token from the database
    token = request.form['token']
    if (database_helper.token_exist(token) == True):
        email = database_helper.get_emailbytoken(token)
        database_helper.remove_token(email)
        _success = True
        _message = "signed out"
    else:
        _message = "user: is not logged in"

    return jsonify(success = _success,
                   message = _message)

# Changes the password of the current user to a new one.
@app.route("/changepassword", methods=['GET', 'POST'])
@cross_origin()
def change_password():
    _success = False
    token = request.args.get('token')
    oldpass = request.args.get('oldPassword')
    newpass = request.args.get('newPassword')

    if (database_helper.token_exist(token) == True):
        # get the email that this token belong to
        email = database_helper.get_emailbytoken(token)
         #check if the email this token belong to has the same pass as oldpass
        if (check_password_hash(database_helper.get_user(email)[6], oldpass) == True):
            # change the password
            phash = generate_password_hash(newpass)
            database_helper.change_password(phash, email)
            _success = True
            _message = "password changed"
        else:
            _message = "old password is wrong"
    else:
        _message = "token doesn't exist, user is not logged in"

    return jsonify(success = _success,
                   message = _message)


# Retrieves the stored data for the user whom the passed token is issued for.
# The currently signed in user can use this method to retrieve all its own information from the server
@app.route("/getuserdatabytoken", methods=['GET', 'POST'])
@cross_origin()
def get_user_data_by_token():
    _success = False
    userinfo = None
    # use this token to look for the email from database(tokens)
    token = request.form['token']
    if (database_helper.token_exist(token) == True):
        email = database_helper.get_emailbytoken(token)
        userinfo = database_helper.get_user(email)
        _message = "Data retrieve success!"
    else:
        _message = "user doesn't exist"

    return jsonify(success = _success,
                   message = _message,
                   object = userinfo)

# Retrieves the stored data for the user specified by the passed email address.
@app.route('/getuserdatabyemail', methods=['GET', 'POST'])
@cross_origin()
def get_user_data_by_email():

    _success = False
    userinfo = None
    token = request.args.get('token')
    email = request.args.get('email')

    if database_helper.token_exist(token) == True:
        if database_helper.user_exist(email) == True:
            userinfo = database_helper.get_user(email)
            _success = True
            _message = "Data retrieve success"
        else:   
            _message = "user doesn't exist"
    else:
        _message = "You cannot do this without login"

    return jsonify(success = _success,
                   message = _message,
                   object = userinfo)

    
    

# Retrieves the stored messages for the user whom the passed token is issued for.
# The currently signed in user can use this method to retrieve all its own messages from th server.
@app.route('/getusermessagebytoken', methods=['GET', 'POST'])
@cross_origin()
def get_user_message_by_token():
    _success = False
    messages = None
    # get email from token
    token = request.args.get('token',)
    if database_helper.token_exist(token) == True:
        email = database_helper.get_emailbytoken(token)
        messages = database_helper.get_message(email)
        _success = True
        _message = "Messages retrieve success"
    else:
        _message = "You cannot get message without login"

    return jsonify(success = _success,
                   message = _message,
                   object = messages)


# Retrieves the stored messages for the user specified by the passed email address.
@app.route("/getusermessagebyemail", methods=['GET', 'POST'])
@cross_origin()
def get_user_message_by_email():
    _success = False
    messages = None
    token = request.args.get('token','')
    # check if the email input is null or not
    if database_helper.token_exist(token) == True:
        email = request.args.get('email','')
        if (database_helper.user_exist(email) == True):
            messages = database_helper.get_message(email)
            _success = True
            _message = "Messages retrieve success"
        else:
            _message = "No such user"
    else:
        _message = "user is not logged in"

    return jsonify(success = _success,
                   message = _message,
                   object = messages)

@app.route("/postmessage", methods=['GET', 'POST'])
@cross_origin()
# Tries to post a message to the wall of the user specified by the email address.
def post_message():
    _success = False
    token = request.args.get('token',) 
    toEmail = request.args.get('toEmail',)
    message = request.args.get('message',)

    if database_helper.token_exist(token) == True:
        fromEmail = database_helper.get_emailbytoken(token)

        if (toEmail == None):
            toEmail = fromEmail
        else:
            pass

        if (database_helper.user_exist(toEmail) == True):
            database_helper.post_message(fromEmail,toEmail,message)
            _success = True
            _message = "Message posted"
        else:
            _message = "no such user"
    else:
        _message = "You are not logged in"

    return jsonify(success = _success,
                   message = _message)

@app.teardown_appcontext
def teardown_connection(exception):
    database_helper.close_db()


if __name__ == "__main__":
    http_server = WSGIServer(('127.0.0.1',5000), app, handler_class=WebSocketHandler)
    http_server.serve_forever()

	

