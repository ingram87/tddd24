# this file shall contain all the server side functions

from flask import Flask, request
import database_helper
import string
import random
from werkzeug.security import generate_password_hash, check_password_hash

# Flask init
app = Flask(__name__)
app.debug = True


#############
# FUNCTIONS #
#############

# generate a 128-bits long token with mixed characters and digits
def token_generator(size=128, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))


# a hello world function, print everything within the database
@app.route('/', methods=['GET'])
def hello():
    return "This is twidder_server"


# Authenticates the username by the provided password.
@app.route("/signin", methods=['POST'])
def sign_in():
    email = request.args.get('email','')
    if (database_helper.user_loggedin(email) == True):
        return "user: %s logged in already" % email
    else:
        if database_helper.user_exist(email) == True:
            # A string value containing a randomly generated access token if the authentication is successful.
            if ((email != '') & (check_password_hash(database_helper.get_user(email)[6], request.args.get('password','')) == True)):
                token = token_generator()
                # save token into the database
                database_helper.add_token(token, email)
                return "this is sign in token: %s" % token
            else:
                return "failed signin"
        else:
            return "user doesn't exist"

# Registers a user in the database.
@app.route("/signup", methods=['POST'])
def sign_up():
    email = request.args.get('email','')
    firstname = request.args.get('firstname','')
    familyname = request.args.get('familyname','')
    gender = request.args.get('gender','')
    city = request.args.get('city','')
    country = request.args.get('country','')
    password = request.args.get('password','')
    phash = generate_password_hash(password)

    if ((email != '') & (firstname != '') & (familyname != '') & (gender != '') & (city != '') & (country != '') & (password != '')):
        # check if user is already signed up
        res = database_helper.user_exist(email)
        if (res == False):
            database_helper.add_user(email, firstname, familyname, gender, city, country, phash)
            return "sign up user: %s" % email
        else:
            return "user already exist"
    else:
        return "some args are empty"

# Signs out a user from the system.
@app.route("/signout", methods=['GET'])
def sign_out():
    #remove the token from the database
    token = request.args.get('token','')
    if (database_helper.token_exist(token) == True):
        email = database_helper.get_emailbytoken(token)
        database_helper.remove_token(email)
        return "signed out"
    else:
        return "user: is not logged in"

# Changes the password of the current user to a new one.
@app.route("/changepassword", methods=['GET'])
def change_password():
    token = request.args.get('token','')
    oldpass = request.args.get('oldPassword','')
    newpass = request.args.get('newPassword','')

    if (database_helper.token_exist(token) == True):
        # get the email that this token belong to
        email = database_helper.get_emailbytoken(token)
         #check if the email this token belong to has the same pass as oldpass
        if (check_password_hash(database_helper.get_user(email)[6], oldpass) == True):
            # change the password
            phash = generate_password_hash(newpass)
            database_helper.change_password(phash, email)
            return "password changed from " + oldpass + " to " + newpass
        else:
            return "old password is wrong" + oldpass
    else:
        return "token doesn't exist, user is not logged in"


# Retrieves the stored data for the user whom the passed token is issued for.
# The currently signed in user can use this method to retrieve all its own information from the server
@app.route("/getuserdatabytoken", methods=['GET'])
def get_user_data_by_token():
    # use this token to look for the email from database(tokens)
    token = request.args.get('token','')
    if (database_helper.token_exist(token) == True):
        email = database_helper.get_emailbytoken(token)
        userinfo = database_helper.get_user(email)
        return "Email: " + userinfo[0] + " Name: " + userinfo[1] + " " + userinfo[2] + " Gender: " + userinfo[3] + " City: " + userinfo[4] + " Country: " + userinfo[5]
    else:
        return "user doesn't exist"

# Retrieves the stored data for the user specified by the passed email address.
@app.route('/getuserdatabyemail', methods=['GET'])
def get_user_data_by_email():

    token = request.args.get('token','')
    email = request.args.get('email','')

    if database_helper.token_exist(token) == True:
        if database_helper.user_exist(email) == True:
            userinfo = database_helper.get_user(email)
            return "Email: " + userinfo[0] + " Name: " + userinfo[1] + " " + userinfo[2] + " Gender: " + userinfo[3] + " City: " + userinfo[4] + " Country: " + userinfo[5]
        else:
            return "user doesn't exist"
    else:
        return "You cannot do this without login"

    
    

# Retrieves the stored messages for the user whom the passed token is issued for.
# The currently signed in user can use this method to retrieve all its own messages from th server.
@app.route('/getusermessagebytoken', methods=['GET'])
def get_user_message_by_token():
    # get email from token
    token = request.args.get('token',)
    if database_helper.token_exist(token) == True:
        email = database_helper.get_emailbytoken(token)
        messages = database_helper.get_message(email)
        return "messages: %s" % messages
    else:
        return "You cannot do this without login"

# Retrieves the stored messages for the user specified by the passed email address.
@app.route("/getusermessagebyemail", methods=['GET'])
def get_user_message_by_email():
    token = request.args.get('token','')
    # check if the email input is null or not
    if database_helper.token_exist(token) == True:

        email = request.args.get('email','')
        if (database_helper.user_exist(email) == True):
            messages = database_helper.get_message(email)
            return "get messages by email: %s" % messages
        else:
            return "No such user: " + email

    else:
        return "user is not logged in"

@app.route("/postmessage", methods=['GET'])
# TESTED
# Tries to post a message to the wall of the user specified by the email address.
def post_message():
    token = request.args.get('token',) 
    toEmail = request.args.get('toEmail',)
    message = request.args.get('message',)

    if database_helper.token_exist(token) == True:
        fromEmail = database_helper.get_emailbytoken(token)

        if (toEmail == None):
            toEmail = fromEmail
        else:
            pass

        if (database_helper.user_exist(toEmail) == True):
            database_helper.post_message(fromEmail,toEmail,message)
            return "message: " + message + " From: " + fromEmail + " To: " + toEmail + " posted."
        else:
            return "no such user: " + toEmail
    else:
        return "You are not logged in"

@app.teardown_appcontext
def teardown_connection(exception):
    database_helper.close_db()


if __name__ == "__main__":
	app.run()

	

